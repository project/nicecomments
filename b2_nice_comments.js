jQuery(document).ready(function(){
    
    jQuery('.b2_nice_comment_textarea').click(function(){
        
        var _form_id = jQuery(this).attr('id').split('___')[1];
        
        jQuery('#'+jQuery(this).attr('id')).hide('fast');
        jQuery('#b2_nice_comment_container_id___'+_form_id).show('fast',function(){

            var container = jQuery('body');
            var scrollTo = jQuery('#comment-form');
        
            container.scrollTop(
                scrollTo.offset().top - container.offset().top
            );
            
        });
        
        jQuery('#b2_nice_comment_container_id___'+_form_id+' '+'textarea[name*="comment_body"]').focus();
        jQuery('#b2_nice_comment_container_id___'+_form_id+' '+'[name="subject"]').focus();
       
    });

    if( location.href.indexOf("#comment-form")!=-1 || location.href.indexOf("comment/reply")!=-1 )
    {
        jQuery('.b2_nice_comment_textarea').click();               
    }
    
});